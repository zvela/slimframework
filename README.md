# Framework

## run app using docker

Start the app using

```bash
cd [my-app-name]
docker-compose up -d
```
(Note: stop mysql service before starting docker)


One liner to stop / remove all of Docker containers:

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```
### Webserver:
After that, open `http://localhost:8080` in your browser.

### Phpadmin
`http://localhost:8081`